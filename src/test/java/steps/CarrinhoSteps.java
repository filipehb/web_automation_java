package steps;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.pt.Dado;
import io.cucumber.java.pt.Entao;
import io.cucumber.java.pt.Quando;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageobject.CarrinhoPageObject;
import pageobject.DetalhesProdutoPageObject;
import pageobject.HomePageObject;
import pageobject.ListaProdutosPageObject;
import utils.Enum;
import utils.NavegadorFactory;

import static org.junit.Assert.assertEquals;

public class CarrinhoSteps {
  public static final int PRIMEIRO_PRODUTO = 0;
  public static final String HOME_PAGE = "https://www.americanas.com.br/";

  @After
  public static void finalizarTeste() {
    NavegadorFactory.fecharNavegador();
  }

  @Before
  public static void iniciarTeste() {
    NavegadorFactory.configurarNavegador(Enum.Navegador.CHROME);
    NavegadorFactory.maximizar();
    NavegadorFactory.acessarPaginaWeb(HOME_PAGE);
  }

  @Dado("que tenha acessado o portal")
  public void que_tenha_acessado_o_portal() {
    HomePageObject homePageObject = new HomePageObject(NavegadorFactory.getDriver());
    WebDriverWait wait = new WebDriverWait(NavegadorFactory.getDriver(), 5);
    wait.until(ExpectedConditions.visibilityOfElementLocated(homePageObject.getSearchButton()));
  }

  @Quando("procuro pelo produto desodorante")
  public void procuro_pelo_produto_desodorante() {
    HomePageObject homePageObject = new HomePageObject(NavegadorFactory.getDriver());
    homePageObject.procurarProduto("desodorante");
  }

  @Quando("seleciono o produto")
  public void seleciono_o_produto() {
    ListaProdutosPageObject listaProdutosPageObject = new ListaProdutosPageObject(NavegadorFactory.getDriver());
    listaProdutosPageObject.clicaNoProdutoNaLista(PRIMEIRO_PRODUTO);
  }

  @Quando("adiciono no carrinho")
  public void adiciono_no_carrinho() {
    DetalhesProdutoPageObject detalhesProdutoPageObject = new DetalhesProdutoPageObject(NavegadorFactory.getDriver());
    assertEquals(detalhesProdutoPageObject.getNomeProdutoLabel().getText(), "Kit com 2 Desodorantes Aerosol Dove Original 100g");
    assertEquals(detalhesProdutoPageObject.getPrecoProdutoLabel().getText(), "R$ 19,99");
    detalhesProdutoPageObject.adicionaProdutoNoCarrinho();
  }

  @Entao("o produto deve estar no carrinho")
  public void o_produto_deve_estar_no_carrinho() {
    CarrinhoPageObject carrinhoPageObject = new CarrinhoPageObject(NavegadorFactory.getDriver());
    assertEquals(carrinhoPageObject.getNomeProdutoLabel().getText(), "Kit Com 2 Desodorantes Aerosol Dove Original 100g");
    assertEquals(carrinhoPageObject.getPrecoProdutoLabel().getText(), "R$ 19,99");
  }
}
