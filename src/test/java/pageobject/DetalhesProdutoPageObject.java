package pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class DetalhesProdutoPageObject {
  private WebDriver driver;
  @FindBy(id = "btn-buy")
  private
  WebElement comprarButton;
  @FindBy(id = "product-name-default")
  private
  WebElement nomeProdutoLabel;
  @FindBy(xpath = ".//span[contains(@class, 'sales-price main-offer__SalesPrice')]")
  private
  WebElement precoProdutoLabel;

  public DetalhesProdutoPageObject(WebDriver driver) {
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    PageFactory.initElements(driver, this);
    this.setDriver(getDriver());
  }

  public WebDriver getDriver() {
    return driver;
  }

  public void setDriver(WebDriver driver) {
    this.driver = driver;
  }

  public WebElement getComprarButton() {
    return comprarButton;
  }

  public void setComprarButton(WebElement comprarButton) {
    this.comprarButton = comprarButton;
  }

  public WebElement getNomeProdutoLabel() {
    return nomeProdutoLabel;
  }

  public void setNomeProdutoLabel(WebElement nomeProdutoLabel) {
    this.nomeProdutoLabel = nomeProdutoLabel;
  }

  public WebElement getPrecoProdutoLabel() {
    return precoProdutoLabel;
  }

  public void setPrecoProdutoLabel(WebElement precoProdutoLabel) {
    this.precoProdutoLabel = precoProdutoLabel;
  }

  public void adicionaProdutoNoCarrinho() {
    getComprarButton().click();
  }
}
