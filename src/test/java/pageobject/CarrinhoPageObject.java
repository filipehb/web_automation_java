package pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class CarrinhoPageObject {
  private WebDriver driver;
  @FindBy(css = ".link-default.clearfix")
  private
  WebElement nomeProdutoLabel;
  @FindBy(css = ".basket-productPrice")
  private
  WebElement precoProdutoLabel;

  public CarrinhoPageObject(WebDriver driver) {
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    PageFactory.initElements(driver, this);
    this.setDriver(driver);
  }

  public WebDriver getDriver() {
    return driver;
  }

  public void setDriver(WebDriver driver) {
    this.driver = driver;
  }

  public WebElement getNomeProdutoLabel() {
    return nomeProdutoLabel;
  }

  public void setNomeProdutoLabel(WebElement nomeProdutoLabel) {
    this.nomeProdutoLabel = nomeProdutoLabel;
  }

  public WebElement getPrecoProdutoLabel() {
    return precoProdutoLabel;
  }

  public void setPrecoProdutoLabel(WebElement precoProdutoLabel) {
    this.precoProdutoLabel = precoProdutoLabel;
  }
}
