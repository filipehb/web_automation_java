package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public class HomePageObject {
  private WebDriver driver;

  @FindBy(id = "h_search-input")
  private WebElement searchInput;
  private By searchButton = By.id("h_search-btn");

  public HomePageObject(WebDriver driver) {
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    PageFactory.initElements(driver, this);
    this.setDriver(driver);
  }

  public WebDriver getDriver() {
    return driver;
  }

  public void setDriver(WebDriver driver) {
    this.driver = driver;
  }

  public WebElement getSearchInput() {
    return searchInput;
  }

  public void setSearchInput(WebElement searchInput) {
    this.searchInput = searchInput;
  }

  public void procurarProduto(String produto) {
    getSearchInput().sendKeys(produto);
    getSearchInput().sendKeys(Keys.ENTER);
  }

  public By getSearchButton() {
    return searchButton;
  }

  public void setSearchButton(By searchButton) {
    this.searchButton = searchButton;
  }
}
