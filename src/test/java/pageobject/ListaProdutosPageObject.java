package pageobject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class ListaProdutosPageObject {
  private static final String PRODUCT_LIST_CSS = ".eAYdMC";
  @FindBy(css = ".eAYdMC")
  List<WebElement> listaProdutos;
  private WebDriver driver;

  public ListaProdutosPageObject(WebDriver driver) {
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    PageFactory.initElements(driver, this);
    this.setDriver(getDriver());
  }

  public WebDriver getDriver() {
    return driver;
  }

  public void setDriver(WebDriver driver) {
    this.driver = driver;
  }

  public List<WebElement> getListaProdutos() {
    return listaProdutos;
  }

  public void setListaProdutos(List<WebElement> listaProdutos) {
    this.listaProdutos = listaProdutos;
  }

  public void clicaNoProdutoNaLista(int index) {
    WebDriverWait wait = new WebDriverWait(this.getDriver(), 10);
    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(PRODUCT_LIST_CSS)));
    getListaProdutos().get(index).click();
  }
}
