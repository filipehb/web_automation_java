# language: pt
@Carrinho
Funcionalidade: Realizar adição de produto no carrinho.

  Cenario: Adicionar produto no carrinho
    Dado que tenha acessado o portal
    Quando procuro pelo produto desodorante
    E seleciono o produto
    E adiciono no carrinho
    Entao o produto deve estar no carrinho